# -*-debian-control-*-
# NOTE: debian/control is generated from debian/templates/control.m4
Source: gnustep-back
Maintainer: Debian GNUstep maintainers <pkg-gnustep-maintainers@lists.alioth.debian.org>
Uploaders:
 Eric Heintzmann <heintzmann.eric@free.fr>,
 Alex Myczko <tar@debian.org>,
 Yavor Doganov <yavor@gnu.org>,
Section: gnustep
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 libcairo2-dev,
 libfontconfig-dev,
 libfreetype-dev,
 libgl-dev,
 libgnustep-gui-dev (>= V_GUI),
 libice-dev,
 libxcursor-dev,
 libxext-dev,
 libxft-dev,
 libxmu-dev,
 libxrandr-dev,
 m4,
 pkgconf,
 texinfo,
Rules-Requires-Root: no
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/gnustep-team/gnustep-back
Vcs-Git: https://salsa.debian.org/gnustep-team/gnustep-back.git
Homepage: http://gnustep.org

Package: gnustep-back`'SOV_BACK
Architecture: all
Multi-Arch: foreign
Depends:
 gnustep-back`'SOV_BACK-cairo | gnustep-back`'SOV_BACK-alt,
 ${misc:Depends},
Suggests:
 fonts-dejavu | fonts-urw-base35,
Provides:
 gnustep-back,
Breaks:
 gnustep-dl2 (<< 0.12.0+git20171224-4),
Description: GNUstep GUI Backend
 It is a backend component for the GNUstep GUI Library.
 The implementation of the GNUstep GUI Library is designed in two parts.
 The first part is the front-end component which is independent of platform
 and display system.  This front-end is combined with a back-end
 component which handles all of the display system dependent such as
 specific calls to the X Window System.
 .
 This is an empty package that depends on the various backends.

Package: gnustep-back`'SOV_BACK-cairo
Architecture: any
Multi-Arch: same
Depends:
 gnustep-back-common (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 gnustep-back`'SOV_BACK-alt,
Description: GNUstep GUI Backend (cairo)
 It is a backend component for the GNUstep GUI Library.
 The implementation of the GNUstep GUI Library is designed in two parts.
 The first part is the front-end component which is independent of platform
 and display system.  This front-end is combined with a back-end
 component which handles all of the display system dependent such as
 specific calls to the X Window System.
 .
 This package provides the cairo backend.

Package: gnustep-back`'SOV_BACK-headless
Architecture: any
Multi-Arch: same
Depends:
 gnustep-back-common (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 gnustep-back`'SOV_BACK-alt,
Description: GNUstep GUI Backend (headless)
 It is a backend component for the GNUstep GUI Library.
 The implementation of the GNUstep GUI Library is designed in two parts.
 The first part is the front-end component which is independent of platform
 and display system.  This front-end is combined with a back-end
 component which handles all of the display system dependent such as
 specific calls to the X Window System.
 .
 This package provides the headless backend which is suitable only for
 testing purposes.

Package: gnustep-back`'SOV_BACK-xlib
Architecture: any
Multi-Arch: same
Depends:
 gnustep-back-common (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 gnustep-back`'SOV_BACK-alt,
Description: GNUstep GUI Backend (xlib)
 It is a backend component for the GNUstep GUI Library.
 The implementation of the GNUstep GUI Library is designed in two parts.
 The first part is the front-end component which is independent of platform
 and display system.  This front-end is combined with a back-end
 component which handles all of the display system dependent such as
 specific calls to the X Window System.
 .
 This package provides the deprecated xlib backend.

Package: gnustep-back-common
Architecture: any
Multi-Arch: foreign
Depends:
 fonts-freefont-ttf,
 ${misc:Depends},
 ${shlibs:Depends},
Description: GNUstep GUI Backend - common files
 It is a backend component for the GNUstep GUI Library.
 The implementation of the GNUstep GUI Library is designed in two parts.
 The first part is the front-end component which is independent of platform
 and display system.  This front-end is combined with a back-end
 component which handles all of the display system dependent such as
 specific calls to the X Window System.
 .
 This package contains the common files needed by the GNUstep GUI Backend.
